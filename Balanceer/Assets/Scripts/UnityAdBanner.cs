﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

public class BannerAds : MonoBehaviour
{

    public string bannerPlacement = "Banner";
    public bool testMode = false;

#if UNITY_IOS
    public const string gameID = "";
#elif UNITY_ANDROID
    public const string gameID = "2896905";
#elif UNITY_EDITOR
    public const string gameID = "";
#endif

    void Start()
    {
        Advertisement.Initialize(gameID, testMode);
        StartCoroutine(ShowBannerWhenReady());
    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.IsReady("banner"))
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(bannerPlacement);
    }
}